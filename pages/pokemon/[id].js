import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import List from '@mui/material/List'
import Typography from '@mui/material/Typography'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import Chip from '@mui/material/Chip'
import Stack from '@mui/material/Stack'
import Link from 'next/link'

import { getDetailPokemon } from '@/app/provider'

export default function Detail() {
  const [pokemonDetail, setPokemonDetail] = useState({})
  
  const router = useRouter()
  const { id } = router.query
  
  useEffect(() => {
    async function datasPokemon(id) {
      const getDetail = await getDetailPokemon(id)

      const urlSplit = getDetail.species !== undefined ? getDetail.species.url.split('/') : [];
      const imageId = urlSplit[6] !== undefined ? urlSplit[6] : 0
      getDetail.image = getDetail.sprites !== undefined && getDetail.sprites.other['official-artwork'].front_default !== null ? getDetail.sprites.other['official-artwork'].front_default : '/image-not-found.png'
      
      let showName = getDetail.forms !== undefined ? getDetail.forms[0].name : ''
      getDetail.showName = showName.toLowerCase().replaceAll('-', ' ').replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
      });
      console.log('Router', getDetail)
      
      setPokemonDetail(getDetail)
    }
    
    if (id !== undefined) {
      datasPokemon(id)
    }
  }, [id])

  return (
    <List sx={{ width: '100%', maxWidth: 510, bgcolor: 'background.paper' }}>
      <Typography variant="h4">{pokemonDetail.showName}</Typography>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <picture>
            <img src={pokemonDetail.image} alt={pokemonDetail.showName} style={{ maxWidth: '200px' }} />
          </picture>
        </Grid>
        <Grid item xs={4}>
          <Typography>Types</Typography>
          <Stack spacing={1} sx={{ textAlign: 'center' }}>
            {pokemonDetail.types !== undefined ? pokemonDetail.types.map((type) => {
              const typeName = type.type.name
              return (
                <>
                  <Chip label={typeName} size="small" />
                </>
              );
            }) : null}
          </Stack>
        </Grid>
        <Grid item xs={4}>
          <Typography>Height</Typography>
          <Stack spacing={1} sx={{ textAlign: 'center' }}>
            <Chip label={pokemonDetail.height / 10 + ' m'} size="small" />
          </Stack>
        </Grid>
        <Grid item xs={4}>
          <Typography>Weight</Typography>
          <Stack spacing={1} sx={{ textAlign: 'center' }}>
            <Chip label={pokemonDetail.weight / 10 + ' kg'} size="small" />
          </Stack>
        </Grid>
        <Grid item xs={12}>
          <Typography>Abilities</Typography>
          <Stack spacing={1} sx={{ textAlign: 'center' }}>
            <Chip sx={{
              height: 'auto',
              '& .MuiChip-label': {
                display: 'block',
                whiteSpace: 'normal',
                lineHeight: '24px',
              },
            }} label={pokemonDetail.abilityEffect} size="small" />
          </Stack>
        </Grid>
        <Grid item xs={12}>
          <Link href={'/'} underline="none" color="inherit">
            <Button variant="contained">Back to pokedex</Button>
          </Link>
        </Grid>
      </Grid>
    </List>
  );
}
