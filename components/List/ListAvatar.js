import * as React from 'react'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import Divider from '@mui/material/Divider'
import ListItemText from '@mui/material/ListItemText'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'
import Stack from '@mui/material/Stack'
import Pagination from '@mui/material/Pagination'
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Link from 'next/link'

export default function AlignItemsList(props) {
  const { datas, types, pagination, handleChangePage, page, totalPage, handleChangeAutocomplete } = props
  const searchPokemon = typeof window !== 'undefined' ? JSON.parse(localStorage.getItem("search_pokemon")) : {}
  
  console.log('Datas', searchPokemon)
  const defaultPropsAutocomplete = {
    options: types,
    getOptionLabel: (option) => option.name,
    isOptionEqualToValue: (option, value) => option.id === value.id
  };

  // const valueDef = { name: 'poison', url:"https://pokeapi.co/api/v2/type/4/" }

  return (
    <List sx={{ width: '100%', maxWidth: 510, minHeight: 580, bgcolor: 'background.paper' }}>
      <Typography variant="h4">Pokedex</Typography>
      <Autocomplete
        {...defaultPropsAutocomplete}
        disablePortal
        id="combo-box-demo"
        sx={{ width: 300 }}
        size='small'
        onChange={handleChangeAutocomplete}
        value={searchPokemon.type}
        renderInput={(params) => <TextField {...params} label="Search by Types" />}
      />
      {datas.map((item, index) => {
        return (
          <>
            <Link href={`/pokemon/${item.name}`} underline="none" color="inherit">
              <ListItem key={index + 1} alignItems="flex-start">
                <ListItemAvatar>
                  <Avatar alt={item.showName} src={item.image} sx={{width: '90px', height: '90px'}} />
                </ListItemAvatar>
                <ListItemText
                  sx={{ marginTop: '37px' }}
                  primary={item.showName}
                />
              </ListItem>
              <Divider variant="inset" component="li" />
            </Link>
          </>
        );
      })}
      {pagination === true ? (
        <Stack spacing={2} sx={{ float: 'right' }}>
          <Pagination count={totalPage} page={page} onChange={handleChangePage} />
        </Stack>
      ) : null}
    </List>
  );
}