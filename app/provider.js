import axios from "axios";

export async function getDataPokemon(offset, limit) {
  try {
    const response = await axios.get(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${limit}`)
    return response.data
  } catch (e) {
    return {
      count: 0,
      results: []
    }
  }
}

export async function getDataTypes() {
  try {
    const response = await axios.get(`https://pokeapi.co/api/v2/type`)
    return response.data
  } catch (e) {
    return {
      count: 0,
      results: []
    }
  }
}

export async function getDetailPokemonTypes(url) {
  try {
    const response = await axios.get(`${url}`)
    return response.data
  } catch (e) {
    return {
      count: 0,
      results: []
    }
  }
}

export async function getDetailPokemon(id) {
  try {
    const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
    const abilities = response.data.abilities[0].ability.url
    const responseAbility = await axios.get(`${abilities}`)
    let abilityEffect = '';
    responseAbility.data.effect_entries.map((effect) => {
      if (effect.language.name === 'en') {
        abilityEffect = effect.effect
      }
    })

    response.data.abilityEffect = abilityEffect;

    // console.log('Detail', response.data)
    return response.data
  } catch (e) {
    return {
      count: 0,
      results: []
    }
  }
}